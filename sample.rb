class Person
  def initialize(args)
    @name = args[:name]
    @age = args[:age] || 1
  end

  def hello
    puts @name * @age.to_i
  end
end

return unless $0 == __FILE__

tom = Person.new(name: "Tom", age: 20)

tom.hello
